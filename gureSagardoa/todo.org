#+title: TODO Index
#+filetags: todo agenda tasks books reading tracking

** Reading
*** Physics/Mathematics [2/10]
**** DONE Seven Wonders of the Cosmos, Jayant V. Narlikar (Physical notes)
**** DONE Introduction to Electrodynamics, David J. Griffiths
**** INPROGRESS A Brief History of Time, Stephen Hawking            :page:41:
**** INPROGRESS The Feynman Lectures on Physics: Vol 1
**** INPROGRESS QED: The Strange Theory of Light & Matter, Feynman  :page:40:
**** TODO The Feynman Lectures on Physics: Vol 2
**** TODO The Feynman Lectures on Physics: Vol 3
**** TODO Quantum, Manjit Kumar
**** TODO Mathematics That Power Our World, Joseph Khoury & Gilles Lamothe
**** TODO Probability, Henk Tijms
*** Computing [9/18]
**** DONE Linux Kernel in a Nutshell, Greg Kroah-Hartman
**** DONE The Hacker's Guide to Python, 3rd Ed., Julien Danjou
**** DONE A Byte of Python, Swaroop C H
**** DONE Tango With Django, Leif Azzopardi & David Maxwell
**** DONE Python Cookbook, 3rd Ed., David Beazley & Brain K. Joned
**** DONE Learning GNU Emacs, 3rd Ed., Debra Cameron, Et al.
**** DONE Writing GNU Emacs Extensions, Bob Glickstein
**** DONE The Linux Kernel Module Programming Guide, Peter Jay Salzman
**** DONE An Introduction to Programming in Emacs Lisp, 3rd Ed., Robert J. Chassell
**** INPROGRESS The Linux Programming Interface, Michael KerrisK
**** INPROGRESS Serious Python, Julien Danjou
**** INPROGRESS Successful Lisp, David B. Lamkins                   :page:88:
**** TODO Cryptography & Network Security, William Stallings
**** TODO Coders at Work: Reflections on the Craft of Programming, Peter Seibel
**** TODO LISP in Small Pieces, Christian Queinnec
**** TODO [[http://www.gigamonkeys.com/book/][Practical Common Lisp]], Peter Seibel
**** TODO ANSI Common LISP, Paul Graham
**** TODO [[https://bigmachine.io/products/the-imposters-handbook/][The Imposter's Handbooks]], Rob Conery
*** Electronics [1/2]
**** DONE Digital Principles and Applications, Albert Paul Malvino & Donald P. Leach
*** Fiction [30/52]
**** DONE The Kite Runner, Khaled Hosseini
**** DONE A Thousand Splendid Suns, Khaled Hosseini
**** DONE The Monk Who Sold His Ferrari, Robin Sharma
**** DONE The Alchemist, Paulo Coelho
**** DONE The Zahir, Paulo Coelho
**** DONE Eleven Minutes, Paulo Coelho
**** DONE Veronika Decides to Die, Paulo Coelho
**** DONE The Winner Stands Alone, Paulo Coelho
**** DONE An Ear to the Ground, James Hadley Chase
**** DONE The Treasure Island, Robert Louis Stevenson
**** DONE Kidnapped, Robert Louis Stevenson
**** DONE The Three Musketeers, Alexandre Dumas
**** DONE The White Tiger, Aravind Adiga
**** DONE The 3 Mistakes of My Life, Chetan Bhagat
**** DONE Five Point Someone, Chetan Bhagat
**** DONE 2 States, Chetan Bhagat
**** DONE One Night @ the Call Center, Chetan Bhagat
**** DONE Being Happy, Andrew Matthews
**** DONE Almost Single, Advaita Kala
**** DONE Nancy Drew: Mystery On Maui, Carolyn Keene
**** DONE The Da Vinci Code, Dan Brown
**** DONE Inferno, Dan Brown
**** DONE Deception Point, Dan Brown
**** DONE Angels & Demons, Dan Brown
**** DONE Digital Fortress, Dan Brown
**** DONE Origin, Dan Brown
**** DONE The Railway Children, Edith Nesbit
**** DONE The Prophet, Kahlil Gibran
**** DONE Jonathan Livingston Seagull, Richard Bach
**** DONE The Hunchback of Notre-Dame, Victor Hugo
**** INPROGRESS GoldFish Have No Hiding Place, James Hadley Chase
**** INPROGRESS The Devil's Alternative, Frederick Forsyth
**** INPROGRESS To Kill A Mocking Bird, Harper Lee
**** INPROGRESS Celestial Bodies, Jokha Alharthi
**** TODO Devils, Fyodor Dostoevsky
**** TODO A Christmas Carol, Charles Dickens
**** TODO Martin Chuzzlewit, Charles Dickens
**** TODO You're Dead Without Money, James Hadley Chase
**** TODO Hit Them Where it Hurts, James Hadley Chase
**** TODO An Ace Up My Sleeve, James Hadley Chase
**** TODO The Whiff of Money, James Hadley Chase
**** TODO Baki Shoonya, Kamalesh Walavalkar
**** TODO The World Set Free, H.G Wells
**** TODO The Apprentice, Arun Joshi
**** TODO Leaves of Grass, Walt Whitman
**** TODO EM and the big HOOM, Jerry Pinto
**** TODO Sabbath's Theater, Philip Roth
**** TODO Lord Jim, Joseph Conrad
**** TODO The Secret Agent, Joseph Conrad
**** TODO And the Mountains Echoed, Khaled Hosseini
**** TODO The Shadow of the Wind, Carlos Ruiz Zafon
**** TODO The Angles Game, Carlos Ruiz Zafon
*** Non Fiction [28/48]
**** DONE Wings of Fire, A P J Abdul Kalam with Arun Tiwari
**** DONE You Are Born To Blossom, A P J Abdul Kalam with Arun Tiwari
**** DONE INDIA 2020: A Vision For the New Millennium, A P J Abdul Kalam with Y S Rajan
**** DONE Amacha Baap Ani Amhi, Narendra Jadhav
**** DONE "Surely You're Joking, Mr. Feynman", Ralph Leighton
**** DONE Shantaram, Gregory David Roberts
**** DONE Black Friday, S. Hussain Zaidi
**** DONE Dongri to Dubai, S. Hussain Zaidi
**** DONE The Class of 83, S. Hussain Zaidi
**** DONE The Element of Style, William, Strunk Jr. & E.B. White
**** DONE Into The Wild, Jon Krakauer
**** DONE Discover your Destiny, Robin Sharma
**** DONE The 5AM Club, Robin Sharma
**** DONE The Greatness guide, Robin Sharma ([[https://gitlab.com/psachin/notes/-/blob/master/the_greatness_guide.org][Notes]])
**** DONE Free as in Freedom, Sam Williams
**** DONE How To Ask Questions The Smart Way, Eric Raymonds & Rick Moen
**** DONE The Art of War, Sun Tzu
**** DONE The Complete Guide to Drones, 2nd Ed., Adam Juniper
**** DONE A long way gone, Ishmael Beah
**** DONE Atomic Habits, James Clear
**** DONE Jeh: A Life of J.R.D. Tata, Bakhtiar K. Dadabhoy
**** DONE Meditations, Marcus Aurelius
**** DONE [[../photography/structures_je_gordon.org][Structures, J. E. Gordan]]
**** DONE The Surrender Experiment, Michael A. Singer
**** DONE The Unthethered Soul, Michael A. Singer
**** DONE As a Man Thinketh, James Allen
**** DONE [[./hnp.org][Hackers & Painters, Paul Graham]]
**** DONE [[./21_lessons.org][21 Lessons for the 21st Century, Yuval Noah Harari]]
**** INPROGRESS A Practical course in Horology, Harold C. Kelly     :page:17:
**** INPROGRESS Guns, Germs, & Steel, Jared Diamond
**** INPROGRESS Longitute, Dava Sobel
**** INPROGRESS [[./theIntelligentInvestor.org][The Intelligent Investor, Benjamin Graham]]
**** TODO Discourses and Selected Writings, Epictetus
**** TODO Letters from a Stoic, Seneca
**** TODO Mein Kampf, Adolf Hitler
**** TODO Joseph Stalin: A Short Biography, G.F Alexandrov, Et al.
**** TODO Medieval India: The Study if a Civilization, Irfan Habib
**** TODO Early India: From the origins to AD 1300, Romila Thapar
**** TODO Tolstoy, Henry Troyat
**** TODO The Checklist Manifesto: How to Get Things Right, Atul Gawande
**** TODO Scandinavia Since 1500, Byron J. Nordstrom
**** TODO The Globalization of Inequality, François Bourguignon
**** TODO A More Perfect Heaven, Dava Sobel
**** TODO Introducing Psychology, Nigel C. Benson
**** TODO Brighter Than a Thousand Suns, Robert Jungk
**** TODO Towards A New Architecture, Le Corbusier
**** TODO The Republic, Plato
**** TODO Autobiography of a Yogi, Paramhansa Yogananda
*** Publications [4/13]
**** DONE [[http://www.kroah.com/linux/talks/ols_2002_kernel_codingstyle_paper/codingstyle.ps][Documentation/Coding Style and Beyond]], Greg Kroah-Hartman
**** DONE [[https://www.nobelprize.org/prizes/physics/1921/einstein/lecture/][Fundamental ideas & problems of the theory of relativity]], A. Einstein
**** DONE [[http://www.flownet.com/gat/papers/lisp-java.pdf][Lisp as an Alternative to Java]], Erann Gat ([[../lisp-java-notes.html][notes]])
**** DONE [[http://www.paulgraham.com/iflisp.html][If Lisp is so Great, Paul Graham]]
**** TODO [[https://www.stat.auckland.ac.nz/~ihaka/downloads/Compstat-2008.pdf][Lisp as a Base for a Statistical Computing System]]
**** TODO Bitcoin: A Peer-to-Peer Electronic Cash System, Satoshi Nakamoto
**** TODO [[http://www.p-cos.net/lisp/guide.html][Pascal Costanza's Highly Opinionated Guide to Lisp]]
**** TODO The structure of the atom(Noble Lecture), Niels Bohr
**** TODO The wave nature of the electron(Noble Lecture), Louis De Broglie
**** TODO [[https://www.nobelprize.org/uploads/2018/06/heisenberg-lecture.pdf][The development of Quantum Mechanics(Noble Lecture)]], W. Heisenberg
**** TODO [[http://lantb.net/uebersicht/wp-pdf/eiffelTower.pdf][The Eiffel Tower, Roland Barthes]]
**** TODO [[http://doc.cat-v.org/feynman/simulating-physics/simulating-physics-with-computers.pdf][Simulating Physics with Computers]], Richard P. Feynman
**** TODO Irving Lavin: [[https://albert.ias.edu/handle/20.500.12111/6675][Michelangelo's Saint Peter's Pieta]]
** Videos
*** YouTube [1/3]
   - [ ] [[https://www.youtube.com/watch?v=At5atF4mKiU][Timeline]]
   - [X] [[https://www.youtube.com/watch?v=lKXe3HUG2l4]["The Mess we're In" by Joe Armstrong.]]
   - [ ] [[https://www.youtube.com/watch?v=TZLvEp_xjnY][LIGO and Gravitational Waves III, Kip S. Thorne (Noble Lecture, 2017)]]
